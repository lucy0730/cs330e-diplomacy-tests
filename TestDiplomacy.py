from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_write, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    # def test_read_1(self):
    #     s = "1 10\n"
    #     i, j = diplomacy_read(s)
    #     self.assertEqual(i, 1)
    #     self.assertEqual(j, 10)
    # # ----
    # # eval
    # # ----

    # def test_eval_1(self):
    #     v = diplomacy_eval(1, 10)
    #     self.assertEqual(v, 20)

    # -----
    # print
    # -----

    # def test_print_1(self):
    #     w = StringIO()
    #     diplomacy_print(w, 1, 10, 20)
    #     self.assertEqual(w.getvalue(), "1 10 20\n")
    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Support C\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC Madrid\n")

    def test_solve_5(self):
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\n")

    def test_solve_6(self):
        r = StringIO(
            "A Houston Move Austin\nB Austin Hold\nC SanAntonio Move Houston\nD Plano Move Houston\nE Dallas Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Houston\nE Dallas\n")

    def test_solve_7(self):
        r = StringIO(
            "A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Beijing\nE NewYork Move Tokyo")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE [dead]\n")

    def test_solve_8(self):
        r = StringIO(
            "A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Tokyo\nE NewYork Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE NewYork\n")

    def test_solve_9(self):
        r = StringIO(
            "A SanDiego Hold\nB Miami Move SanDiego\nC LasVegas Move SanDiego\nD Atlanta Support B\nE NewOrleans Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Atlanta\nE NewOrleans\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()
