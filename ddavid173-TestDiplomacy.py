from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read, cities

class TestDiplomacy (TestCase):
    
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\n")
    
    def test_solve3(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve4(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\nC Austin Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\nC Austin\n")

    def test_solve5(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\nC Austin Support A\nD London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB [dead]\nC Austin\nD London\n")

    def test_solve6(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\nC Austin Support B\nD London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC Austin\nD London\n")

    def test_read1(self):
        s = "A Madrid Move Barcelona"
        loc, army, action, dest = diplomacy_read(s)
        self.assertEqual(loc, "Madrid")
        self.assertEqual(army, "A")
        self.assertEqual(action, "Move")
        self.assertEqual(dest, "Barcelona")

    def test_read2(self):
        s = "A London Hold"
        loc, army, action, dest = diplomacy_read(s)
        self.assertEqual(loc, "London")
        self.assertEqual(army, "A")
        self.assertEqual(action, "Hold")
        self.assertEqual(dest, None)

    def test_read3(self):
        s = "A London Support B"
        loc, army, action, dest = diplomacy_read(s)
        self.assertEqual(loc, "London")
        self.assertEqual(army, "A")
        self.assertEqual(action, "Support")
        self.assertEqual(dest, "B")

if __name__ == "__main__":
    main()